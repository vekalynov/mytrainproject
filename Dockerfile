# в качестве базового образа используем java:8,
#добавляем наш Java-класс (команда ADD),
# компилируем его (при помощи команды RUN)
# и указываем команду, которая выполнится при запуске контейнера (команда CMD)
FROM openjdk:8-jdk-alpine
COPY /target/myTrain-1.0-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]